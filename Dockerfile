## First Stage: Build Application
FROM node:18 as build

WORKDIR /app

COPY . .

## Rebuild platform-dependent dependencies
RUN yarn rebuild

## Build the project
RUN yarn build

## Second Stage: Production Image
FROM nginx:1.25-alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/build /app
RUN chown -Rv nginx:nginx /app
