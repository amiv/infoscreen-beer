import React, { useState } from 'react'
import useShowProst from './hooks/useShowProst'
import useApiPosters from './hooks/useApiPosters'
import useDadJoke from './hooks/useDadJoke'
import './App.css'

const API_URL = 'https://api.amiv.ethz.ch/'
const POLLING_INTERVAL = 1000 * 60 * 5 // 5 Minute
const CHANGE_INTERVAL = 10000 // 10 Seconds
const DEFAULT_POSTER_INTERVAL = 3 // default poster is shown every third poster
const DEFAULT_BANNER_INTERVAL = 4 // default banner is shown every fourth poster to avoid displaying it in the same pattern wrt. the default poster
const DEFAULT_POSTER_URL = '/poster.jpg'
const DEFAULT_BANNER_URL = '/banner.jpg'
const DEFAULT_PROST_IMAGE = '/prost.jpg'

function App() {
  const [index, setIndex] = useState(0)
  const showProstPoster = useShowProst()
  const [showDefaultPoster, setShowDefaultPoster] = useState(true)
  const [showDefaultBanner, setShowDefaultBanner] = useState(true)
  const [currentPosterUrl, setCurrentPosterUrl] = useState(DEFAULT_POSTER_URL)
  const [nextPosterUrl, setNextPosterUrl] = useState(undefined)
  const images = useApiPosters(API_URL, POLLING_INTERVAL)
  const [quote, setQuote] = useState('')
  const dadJoke = useDadJoke(CHANGE_INTERVAL)
  const [iFrameKey, setIFrameKey] = useState(false)

  const getNextIndex = (currentIndex) => {
    if (currentIndex >= images.length - 1) {
      return 0
    }
    return currentIndex + 1
  }

  function changePoster() {
    if (showDefaultPoster) {
      setShowDefaultPoster(false)
      setCurrentPosterUrl(images[index] || DEFAULT_POSTER_URL)
      setNextPosterUrl(images[getNextIndex(index)])
      return
    }

    const newIndex = getNextIndex(index)

    if (newIndex % DEFAULT_POSTER_INTERVAL === 0 || images.length === 0) {
      setShowDefaultPoster(true)
      setCurrentPosterUrl(DEFAULT_POSTER_URL)
      setNextPosterUrl(images[newIndex])
      setIFrameKey(!iFrameKey) // toggle everytime the Default poster is shown to bridge the re-rendering time
    } else {
      setCurrentPosterUrl(images[newIndex] || DEFAULT_POSTER_URL)
      setNextPosterUrl(images[getNextIndex(newIndex)])
    }

    if (!showDefaultPoster && newIndex % DEFAULT_BANNER_INTERVAL === 0) {
      setShowDefaultBanner(true)
    } else {
      setShowDefaultBanner(false)
    }
    setQuote(dadJoke)
    setIndex(newIndex)
  }

  setTimeout(() => changePoster(), CHANGE_INTERVAL)

  return (
    <div className="App">
      <img
        key={DEFAULT_PROST_IMAGE}
        className={showProstPoster ? 'prostPoster' : undefined}
        src={DEFAULT_PROST_IMAGE}
      />
      <img
        key={DEFAULT_POSTER_URL}
        className={showDefaultPoster && !showProstPoster ? 'selected' : undefined}
        src={DEFAULT_POSTER_URL}
      />
      <iframe
        key={iFrameKey}
        className={!showDefaultPoster && !showDefaultBanner ? 'weather' : undefined}
        src="https://search.ch/meteo/widget/Zurich?bgcolor=000000&amp;fgcolor=ffffff"
      ></iframe>
      <p className={!showDefaultPoster && !showDefaultBanner ? 'banner' : undefined}>{quote}</p>
      {currentPosterUrl !== DEFAULT_POSTER_URL && (
        <img
          key={currentPosterUrl}
          className={!showProstPoster ? 'selected' : undefined}
          src={currentPosterUrl}
        />
      )}
      {nextPosterUrl && nextPosterUrl !== currentPosterUrl && (
        <img key={nextPosterUrl} src={nextPosterUrl} />
      )}
      <img
        key={DEFAULT_BANNER_URL}
        className={!showDefaultPoster && showDefaultBanner ? 'banner' : undefined}
        src={DEFAULT_BANNER_URL}
      />
    </div>
  )
}

export default App
