import { useEffect, useState, useRef } from 'react'

// Custom Hook which enables us to use intervals
const useAdvancedInterval = (callback, delay) => {
  const [lastTime, setLastTime] = useState(new Date())
  const [isPaused, setIsPaused] = useState(false)
  const [remainingTime, setRemainingTime] = useState(delay)
  const [timeoutHandle, setTimeoutHandle] = useState(null)
  const savedCallback = useRef()

  function clear() {
    if (timeoutHandle) {
      clearTimeout(timeoutHandle)
      setTimeoutHandle(null)
    }
  }

  function resume() {
    setIsPaused(false)
  }

  function pause() {
    setIsPaused(true)
  }

  function startTimeout(time) {
    function tick() {
      const execStartTime = new Date()
      setLastTime(execStartTime)

      if (savedCallback.current) {
        savedCallback.current()
      }

      // Start another timeout for the next iteration
      startTimeout(Math.max(0, delay - (new Date() - execStartTime)))
    }

    setTimeoutHandle(setTimeout(tick, time || delay))
    setRemainingTime(null)
  }

  useEffect(() => {
    if (isPaused) {
      setRemainingTime(delay - (new Date() - lastTime))
      clear()
    } else {
      startTimeout(remainingTime)
    }
  }, [isPaused])

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  useEffect(() => {
    if (isPaused && remainingTime > delay) {
      setRemainingTime(delay)
    }
  }, [delay])

  return [isPaused, pause, resume]
}

export default useAdvancedInterval
