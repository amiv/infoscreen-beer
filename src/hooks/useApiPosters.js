import { useState } from 'react'
import axios from 'axios'
import useInterval from './useInterval'

// Custom hook to get the list of posters (which gets regularly updated)
const useApiPosters = (apiUrl, pollingInterval) => {
  const [images, setImages] = useState([])

  useInterval(
    () => {
      // prettier-ignore
      (async () => {
            try {
                const date = `${new Date().toISOString().split('.')[0]}Z`
                const result = await axios.get(`${apiUrl}/events?where={
                            "show_infoscreen": true,
                            "time_advertising_start": {"$lte":"${date}"},
                            "time_advertising_end": {"$gte":"${date}"},
                            "img_poster":{"$ne":null}}`)

                const img_list = result.data._items.map((e) => `${apiUrl}${e.img_poster.file}`)
                setImages(img_list)
            } catch (err) {
                console.error(err)
            }
            })()
    },
    pollingInterval,
    true
  )

  return images
}

export default useApiPosters
