import { useState } from 'react'
import useInterval from './useInterval'

const useDadJoke = (pollingInterval) => {
  const [joke, setJoke] = useState(null)

  useInterval(
    () => {
      const fetchJoke = async () => {
        const apiUrl = 'https://icanhazdadjoke.com/'
        try {
          const response = await fetch(apiUrl, {
            method: 'GET',
            headers: {
              Accept: 'application/json',
            },
          })
          if (response.ok) {
            const responseData = await response.json()
            setJoke(responseData.joke)
            console.log(responseData)
          } else {
            console.error('Failed to fetch joke:', response.statusText)
          }
        } catch (error) {
          console.error('Error during fetch:', error)
        }
      }
      fetchJoke()
    },
    pollingInterval,
    true
  ) // Empty dependency array means this effect runs once, similar to componentDidMount
  return joke
}

export default useDadJoke
