import { useEffect, useRef } from 'react'

// Custom Hook which enables us to use intervals
const useInterval = (callback, delay, execFirst = false) => {
  const savedCallback = useRef(callback)

  useEffect(() => {
    function tick() {
      if (savedCallback.current) {
        savedCallback.current()
      }
    }

    if (execFirst) {
      tick()
    }

    const interval = setInterval(tick, delay)

    return () => clearInterval(interval)
  }, [delay])

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback
  }, [callback])
}

export default useInterval
