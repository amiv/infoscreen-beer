import { useEffect, useState } from 'react'

// Custom Hook for showing the prost poster
const useShowProst = () => {
  const [showProstPoster, setShowProstPoster] = useState(false)

  let ws

  const checkWSConnectivity = () => {
    if (ws === undefined || ws.readyState === ws.CLOSED) {
      ws = new WebSocket('ws://127.0.0.1:1234')
      ws.onmessage = (event) => {
        if (event.data === 'beer_triggered') {
          console.log('Prost-event triggered')
          setShowProstPoster(true)
          setTimeout(() => setShowProstPoster(false), 2000)
        } else {
          console.log('Received: ', event.data)
        }
      }
    }
  }

  useEffect(() => {
    setInterval(checkWSConnectivity, 10000)
  }, [])

  return showProstPoster
}

export default useShowProst
